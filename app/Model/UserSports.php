<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserSports extends Model
{
    protected $fillable = [ 'user_id', 'sport_id', 'days_spent' ];

    protected $primaryKey = 'user_id';


    public function sport()
    {
        return $this->hasOne(Sports::class, 'id', 'sport_id');
    }
    
    public function user()
    {
    	return $this->hasOne(User::class, 'id', 'user_id');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserMessages extends Model
{
    protected $fillable = [ 'user_id', 'r_user_id', 'message' ];
	
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
	
	public function r_user()
	{
		return $this->belongsTo(User::class, 'r_user_id', 'id');
	}
	
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sports extends Model
{
    protected $fillable = ['id', 'name'];
}

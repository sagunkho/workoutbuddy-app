<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserProfile extends Model
{
    protected $fillable = [ 
    	"id", "biography", "first_name", "last_name", "gender", "age",
    	"city", "country"
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, "id", "id");
    }
}

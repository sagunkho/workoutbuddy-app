<?php

namespace App\Http\Controllers;

use App\UserProfile;
use Illuminate\Http\Request;
use App\Model\Sports;
use App\Model\UserMessages;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userlist = UserProfile::paginate(15);
	    
        return view('home', [
            'userlist' => $userlist
        ]);
    }
    
    public function searchResult(Request $request)
    {
    	if ($request->get('sport')) {
		    $sports = \App\Model\Sports::whereIn('id', $request->get('sport'))->get();
		    $user_sports = \App\Model\UserSports::whereIn('sport_id', $request->get('sport'))->get();
		    $userlist = collect();
		    foreach($user_sports->groupBy('user_id')->flatten() as $usport) {
		    	if ($usport->user->profile != null)
			    	$userlist->push($usport->user->profile);
		    }
	    } else if ($request->get('name')) {
	    	$userlist = \App\UserProfile::where('first_name', 'like', "%{$request->get('name')}")
	            ->orWhere('last_name', 'like', "%{$request->get('name')}%")->get();
	    }
	    
    	return view('user.search_result', [
		    'userlist' => $userlist
	    ]);
    }
    
    public function profile(Request $request)
    {
        $user = $request->user();

        $sports = Sports::all()->toArray();

    	return view('user.profile', [
            'user' => $user,
            'sports' => $sports
        ]);
    }

    public function editProfile(Request $request)
    {
        $user = $request->user();
        $data = $request->all();

        $this->validate($request, [
	        'biography' => 'filled|min:4',
	        'first_name' => 'filled|min:4',
	        'last_name' => 'filled|min:4',
	        'gender' => 'filled',
	        'city' => 'filled',
	        'country' => 'filled'
        ]);

        if ($user->profile == null) {
            $user->profile()->create([
                'biography' => $data['biography'],
                'first_name' => $data['first_name'],
                'age' => $data['age'],
                'last_name' => $data['last_name'],
                'gender' => $data['gender'],
                'city' => $data['city'],
                'country' => $data['country']
            ]);
        } else {
            $profile = $user->profile;
            $profile->biography = $data['biography'];
            $profile->first_name = $data['first_name'];
            $profile->age = $data['age'];
            $profile->last_name = $data['last_name'];
            $profile->gender = $data['gender'];
            $profile->city = $data['city'];
            $profile->country = $data['country'];
            $profile->save();
        }

		session()->flash('alert-success', 'Profile successfully edited!');

        return redirect()->back();
    }

    public function editWorkoutProfile(Request $request)
    {
        $user = $request->user();
        $data = $request->all();

		$sports = Sports::all();

		$user_sport = null;
		foreach($sports as $sport)
		{
			$days = 0;
			$found = false;
			$user_sport = $user->sports()->where('sport_id', $sport->id)->first();

			$key = array_search($sport->id, $data['sport']);

			// found.
			$days = $data['sport_days'][$key];

			if (!isset($data['sport_days'][$key]) || $data['sport_days'][$key] <= 0 || $data['sport_days'][$key] == null)
				$days = 0;

			if ($key === false && $user_sport != null) {
				$user_sport->delete();
			} else if ($key !== false && $user_sport != null) {
				$user_sport->days_spent = $days;
				$user_sport->save();
			} else if ($key !== false && $user_sport == null) {
				$user->sports()->create([
					'sport_id' => $sport->id,
					'days_spent' => $days
				]);
			}
		}

        session()->flash('alert-success', 'Workout profile successfully edited!');

        return redirect()->back();
    }
    
    public function roulette(Request $request)
    {
    	$user = request()->user();
	    
	    $userlist = UserProfile::all();
	    
	    $idarr = $userlist->pluck('id');
	    $id = $idarr[mt_rand(0, $idarr->count()-1)];
	    
	    // Select random user.
	    do {
		    $r_user = $userlist->where('id', $id)->flatten()[0];
	    } while ($r_user->id == $user->id);
	    
    	return view('user.roulette', [
		    	'r_user' => $r_user,
	    ]);
    }
    
    public function workoutRequest(Request $request)
    {
	    $r_user = UserProfile::where('id', $request->get('r_user_id'))->first();
	    
	    if ($r_user == null) {
	    	return redirect()->back();
	    }
	    
	    $sender_messages = UserMessages::where('user_id', request()->user()->id)
		        ->where('r_user_id', $r_user->id)
		        ->orderBy('created_at', 'desc')
		        ->get();
	    
	    $received_messages = UserMessages::where('user_id', $r_user->id)
		        ->where('r_user_id', request()->user()->id)
		        ->orderBy('created_at', 'desc')
		        ->get();
	    
	    $conversation = $sender_messages->merge($received_messages)->sortByDesc('created_at');
	    
    	return view('user.workout_request', [
    		'user' => request()->user(),
		    'r_user' => $r_user,
		    'conversation' => $conversation
	    ]);
    }
    
    public function workoutRequestSend(Request $request)
    {
    	$data = $request->all();
	    $user = request()->user();
	    
	    $this->validate($request, [
	    	'message' => 'required|filled|min:10|max:255',
		    'r_user_id' => 'exists:users,id'
	    ]);
	    
	    session()->flash('alert-success', 'Your message was successfully sent!');
	    
	    $user->sent_messages()->create([
		    'user_id' => $user->id,
		    'r_user_id' => $data['r_user_id'],
		    'message' => $data['message']
	    ]);
	    
	    return redirect()->back();
    }
}

<?php

namespace App;

use App\Model\UserSports;
use App\Model\UserMessages;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(UserProfile::class, 'id', 'id');
    }

    public function sports()
    {
        return $this->hasMany(UserSports::class, 'user_id','id');
    }
    
    public function sent_messages()
    {
    	return $this->hasMany(UserMessages::class, 'user_id', 'id');
    }
    
    public function received_messages()
    {
    	return $this->hasMany(UserMessages::class, 'user_id', 'id');
    }
    
}

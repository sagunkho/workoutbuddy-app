<?php

use Illuminate\Database\Seeder;

class SportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
    		[ 'name' => 'Cycling' ],
    		[ 'name' => 'Football' ],
    		[ 'name' => 'Basketball' ],
    		[ 'name' => 'Baseball' ],
    		[ 'name' => 'Swimming' ],
    		[ 'name' => 'Badminton' ],
    		[ 'name' => 'Tennis' ],
    		[ 'name' => 'Cricket' ],
    		[ 'name' => 'Cardio' ],
    		[ 'name' => 'Running' ],
    		[ 'name' => 'Weight Lifting' ],
    		[ 'name' => 'Strenching' ],
    		[ 'name' => 'Rock Climbing' ],
		    [ 'name' => 'Surfing' ],
		    [ 'name' => 'Ice Hockey' ],
		    [ 'name' => 'Skating' ],
		    [ 'name' => 'Skiing' ],
		    [ 'name' => 'Pilates' ],
		    [ 'name' => 'Yoga' ],
		    [ 'name' => 'Judo' ],
		    [ 'name' => 'Taekwondo' ],
		    [ 'name' => 'Karate' ],
		    [ 'name' => 'Wrestling' ],
		    [ 'name' => 'Kung-fu' ],
		    [ 'name' => 'Kayaking' ],
		    [ 'name' => 'American Football' ],
		    [ 'name' => 'Table Tennis' ],
		    [ 'name' => 'Boxing' ],
		    [ 'name' => 'Golf' ],
		    [ 'name' => 'Gymnastics' ],
		    [ 'name' => 'Quiddich' ],
    	];

    	DB::table('sports')->insert($data);
    }
}

<?php

use Illuminate\Database\Seeder;

class UserListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $sports = \App\Model\Sports::all();

        for ($i = 0; $i < 100; $i++) {
	        $user = new \App\User();
	
	        $user->create([
		        'name' => $faker->name,
		        'email' => $faker->email,
		        'password' => bcrypt($faker->password),
	        ])->profile()->create([
		        'biography' => $faker->sentence,
		        'first_name' => $faker->firstName,
		        'last_name' => $faker->lastName,
		        'age' => mt_rand(1, 100),
		        'gender' => mt_rand(0, 1) ? "MALE" : "FEMALE",
		        'city' => $faker->city,
		        'country' => $faker->country
	        ]);
        }
    }
}

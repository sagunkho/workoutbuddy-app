<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->integer('id')->unsigned()->unique();
            
            $table->string('biography')->default('');
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->enum('gender', ['MALE', 'FEMALE' ])->default('male');
            $table->integer('age')->default(0);
            $table->string('city')->default('');
            $table->string('country')->default('');

            $table->timestamps();

            $table->foreign('id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Route::post('/searchResult', 'HomeController@searchResult')->name('searchResult');
Route::get('/profile', 'HomeController@profile')->name('profile');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/edit_profile', 'HomeController@editProfile')->name('editProfile');
Route::post('/edit_workout_profile', 'HomeController@editWorkoutProfile')->name('editWorkoutProfile');

Route::get('/roulette', 'HomeController@roulette')->name('roulette');

Route::get('/workoutRequest', 'HomeController@workoutRequest')->name('workoutRequest');
Route::post('/workoutRequestSend', 'HomeController@workoutRequestSend')->name('workoutRequestSend');
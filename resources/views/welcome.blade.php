<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Gym Buddy App - Make friends to workout with!</title>

	    <!-- Stylesheet -->
	    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    </head>
    <body>
        @include('layouts.navigation_bar')

        @if (Auth::guest())
        <div class="flex-center position-ref full-height welcome-bg">
            <div class="content">
	            <div class="register-form">
		            <h3 style="text-align: center;">Find a Gym Buddy!</h3>
		            <hr>
		            <form class="form-horizontal" style="padding: 20px;" method="POST" action="{{ route('register') }}">
			            {{ csrf_field() }}

			            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
				            <label for="name" class="col-md-4 control-label">Name</label>

				            <div class="col-md-6">
					            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

					            @if ($errors->has('name'))
						            <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
					            @endif
				            </div>
			            </div>

			            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

				            <div class="col-md-6">
					            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

					            @if ($errors->has('email'))
						            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
					            @endif
				            </div>
			            </div>

			            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				            <label for="password" class="col-md-4 control-label">Password</label>

				            <div class="col-md-6">
					            <input id="password" type="password" class="form-control" name="password" required>

					            @if ($errors->has('password'))
						            <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
					            @endif
				            </div>
			            </div>

			            <div class="form-group">
				            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

				            <div class="col-md-6">
					            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
				            </div>
			            </div>

			            <div class="form-group">
				            <div class="col-md-6 col-md-offset-4">
					            <button type="submit" class="btn btn-primary">
						            Register
					            </button>
				            </div>
			            </div>
		            </form>
	            </div>
            </div>
        </div>
		@else
        <div class="content">
			<div class="page">

			</div>
        </div>
		@endif
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>

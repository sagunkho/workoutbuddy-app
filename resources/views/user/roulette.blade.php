@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2" style="height: 300px; background-image: url({{asset('images/person.jpg')}}); background-size: 100% 100%; background-repeat: no-repeat;">
				<span style="font-size: 24px; font-weight: bold; color: #fff; text-shadow: 1px 1px 2px rgba(150, 150, 150, 1); position: absolute; bottom: 20px; left: 20px;">
					{{$r_user->first_name}} {{$r_user->last_name}} <small>({{$r_user->age}})</small>
				</span>
				<a href="{{route('workoutRequest', [ 'r_user_id' => $r_user->id ])}}">
					<button class="btn btn-default" style="position: absolute; bottom: 20px; right: 20px;">
						Message
					</button>
				</a>
			</div>
			<div class="col-md-8 col-md-offset-2" style="background: #fff;">
				<div style="padding: 10px;">
					{{$r_user->biography}}

					<h3>Workouts</h3>
					<hr style="margin-top: 0px; margin-bottom: 5px;">

					@if ($r_user->user->sports->count())
						@foreach($r_user->user->sports as $sport)
							<strong>{{$sport->sport->name}}</strong> with {{$sport->days_spent}} days of experience.<br>
						@endforeach
					@else
						None.
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection
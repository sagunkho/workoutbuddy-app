@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						Send a message to {{$r_user->first_name}}
					</div>
					<form role="form" action="{{route('workoutRequestSend', [ 'r_user_id' => $r_user->id ])}}" method="POST">
						{{ csrf_field() }}

						<div class="panel-body">
							@if ($errors->any())
								<div class="alert alert-danger">
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							@if (session('alert-success'))
								<div class="alert alert-success">
									{{ session('alert-success') }}
								</div>
							@endif
							<textarea class="form-control" rows="5" name="message" id="message" Placeholder="Your message...">Hi {{$r_user->first_name}}! I want to workout with you.</textarea>
						</div>

						<div class="panel-footer" align="right">
							<a href="{{route('home')}}">
								<button class="btn btn-default">Cancel</button>
							</a>
							<button class="btn btn-success" type="submit">Send Request</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-6 col-md-offset-3">
				@foreach($conversation as $message)
				<div class="panel panel-default">
					<div class="panel-heading">
							{{$message->r_user->name}}
						<small class="text-muted" style="position: absolute; right: 30px;">{{$message->created_at->diffForHumans()}}</small>
					</div>
					<div class="panel-body">
						{{$message->message}}
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection
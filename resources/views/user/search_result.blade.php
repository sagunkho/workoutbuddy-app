@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2"></div>
		@foreach($userlist as $user)
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong>{{$user->first_name . ' ' . $user->last_name}}</strong>
					<span class="text-muted small">{{$user->age}} years old</span>
				</div>
				<div class="panel-body">
					{{$user->first_name}}@if ($user->user->sports->count() == 0) does not workout. @else partakes in @endif
					@foreach($user->user->sports as $sport)
						{{$sport->sport->name}}@if (!$loop->last), @else. @endif
					@endforeach
				</div>
			</div>
		@endforeach
	</div>
</div>
@endsection
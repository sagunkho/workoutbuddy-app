@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				@if (session('alert-success'))
				<div class="alert alert-success">
					{{ session('alert-success') }}
				</div>
				@endif

				@if(Auth::user()->email == $user->email)
					<div>
						<button class="btn btn-default" data-toggle="modal" data-target="#profile_edit_modal">
							Edit Profile
						</button>
					</div>
				@endif

				<div class="profile-image" style="text-align: center;">
					<img src="images/person.jpg" style="border-radius: 50%; height: 250px; width:250px;" />
				</div>

				<div class="panel panel-default" style="margin-top: 20px;">
					<div class="panel-heading">Biography</div>

					<div class="panel-body">
						@if ($user->profile != null)
							{{$user->profile->biography}}
						@else
							I'm the best! Workout with me.
						@endif
					</div>
				</div>

				<div class="panel panel-default" style="margin-top: 20px;">
					<div class="panel-heading">Basic Profile</div>

					<div class="panel-body">
						@if (session('status'))
							<div class="alert alert-success">
								{{ session('status') }}
							</div>
						@endif

						<table class="table table-condensed table-responsive table-bordered">
							<tr>
								<th>Full Name</th>
								<td>@if($user->profile == null) {{$user->name}} @else {{$user->profile->first_name . ' ' . $user->profile->last_name}} @endif </td>
							</tr>
							<tr>
								<th>Gender</th>
								<td>@if($user->profile == null) Unknown  @else {{$user->profile->gender}} @endif</td>
							</tr>
							<tr>
								<th>Age</th>
								<td>@if($user->profile == null) Unknown  @else {{ $user->profile->age }} @endif</td>
							</tr>
							<tr>
								<th>City, Country</th>
								<td>@if($user->profile == null) Unknown  @else {{$user->profile->city .', ' . $user->profile->country}} @endif</td>
							</tr>
						</table>
					</div>
				</div>

				<div class="panel panel-default" style="margin-top: 20px;">
					<div class="panel-heading">Workout Profile <button class="btn btn-default" data-toggle="modal" data-target="#edit_workout_profile_modal" style="float: right;">Edit</button> <div class="clearfix"></div></div>

					<div class="panel-body">
						@if (session('status'))
							<div class="alert alert-success">
								{{ session('status') }}
							</div>
						@endif

						<h3>Workout Profile</h3>
						<table class="table table-condensed table-bordered">
							@foreach($user->sports as $user_sport)
								<?php
									$name = \App\Model\Sports::find($user_sport->sport_id)->name;
								?>
								<tr>
									<th>{{$name}}</th>
									<td>Experience: {{$user_sport->days_spent}} days</td>
								</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div style="padding: 20px;"></div>
		</div>
	</div>

	@if (Auth::user()->email == $user->email)
	<!-- Modal -->
	<div id="profile_edit_modal" class="modal fade" role="dialog">
		<div class="modal-dialog">

		<!-- Modal content-->
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Profile</h4>
				</div>
				<form action="{{route('editProfile')}}" role="form" method="POST">
					{{ csrf_field() }}
					<!-- Modal Body -->
					<div class="modal-body">
						<p>Edit your biography by filling the form below.</p>
						<!-- Biography -->
						<div class="form-group">
							<label for="biography">Biography: </label>
							<input type="text" name="biography" class="form-control" id="biography" aria-describedby="biography" value="{{($user->profile == null ? "" : $user->profile->biography)}}" placeholder="Biography" />
						</div>

						<!-- Full Name -->
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label for="first_name">First Name: </label>
							<input type="text" name="first_name" class="form-control" id="first_name" aria-describedby="first_name"  value="{{($user->profile == null ? "" : $user->profile->first_name)}}" placeholder="First Name" />
						</div>

						<!-- Last Name -->
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label for="last_name">Last Name: </label>
							<input type="text" name="last_name" class="form-control" id="last_name" aria-describedby="last_name"  value="{{($user->profile == null ? "" : $user->profile->last_name)}}" placeholder="Last Name" />
						</div>

						<!-- Age -->
						<div class="form-group col-md-6 col-sm-6 col-xs-6">
							<label for="age">Age: </label>
							<input type="number" name="age" class="form-control" id="age" aria-describedby="age"  value="{{($user->profile == null ? "" : $user->profile->age)}}" placeholder="age" />
						</div>

						<div class="clearfix"></div>

						<!-- Gender -->
						<div class="form-group">
							<label for="gender">Gender: </label>
							<select name="gender">
								<option value="MALE" @if ($user->profile != null && $user->profile->gender == "MALE") selected @endif>Male</option>
								<option value="FEMALE" @if ($user->profile != null && $user->profile->gender == "FEMALE") selected @endif>Female</option>
							</select>
						</div>

						<!-- City -->
						<div class="form-group">
							<input type="text" name="city" class="form-control" aria-describedby="city" placeholder="City" value="{{($user->profile == null ? "" : $user->profile->city)}}"  />
						</div>
						<!-- Country -->
						<div class="form-group">
							<input type="text" name="country" class="form-control" aria-describedby="country" placeholder="Country" value="{{($user->profile == null ? "" : $user->profile->country)}}" />
						</div>
					</div>
					<!-- Modal Footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save Profile</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endif

	@if (Auth::user()->email == $user->email)
	<!-- Modal -->
	<div id="edit_workout_profile_modal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Profile</h4>
				</div>
				<form action="{{route('editWorkoutProfile')}}" role="form" method="POST">
					{{ csrf_field() }}
					<!-- Modal Body -->
					<div class="modal-body">
						<h3>Which sports do you play? </h3>
						<div>
							<table class="table table-bordered">
							@foreach($sports as $key => $sport)
								<tr>
									<td>
										<?php $user_sport = $user->sports->where('sport_id', $sport['id'])->first(); ?>
										<input type="checkbox" name="sport[{{$key}}]" value="{{$sport["id"]}}" id="sport_{{$key}}"
										@if ($user_sport != null) checked @endif/>&nbsp;
										<label for="sport_{{$key}}">{{$sport["name"]}}</label>
									</td>
									<td>
										<input type="number" name="sport_days[{{$key}}]" value="{{($user_sport != null ?
											$user_sport->days_spent : "")}}" id="sport_days_{{$key}}" />
										<label for="sport_days_{{$key}}">Days spent</label>
									</td>
								</tr>
							@endforeach
							</table>
						</div>
					</div>
					<!-- Modal Footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save Profile</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	@endif
@endsection

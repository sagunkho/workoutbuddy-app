@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
	        @if (session('status'))
	            <div class="alert alert-success">
	                {{ session('status') }}
	            </div>
	        @endif

	        <h3 style="text-align: center;">We have {{\App\UserProfile::all()->count()}} Members</h3>


		        <div class="panel panel-default">
			        <div class="panel-heading">
				        Find a workout-buddy
			        </div>
			        <div class="panel-body">
						<form role="form" method="POST" action="{{route('searchResult')}}">
							{{csrf_field()}}
							<div class="form-group">
								<input type="text" name="name" id="name" class="form-control" placeholder="Search for a member by name" />
							</div>
							@foreach(App\Model\Sports::all() as $sport)
								<div class="col-md-3">
									<input type="checkbox" name="sport[{{$loop->index}}]" value="{{$sport->id}}" @if(request()->get($sport->id)) selected @endif/>
									<label for="{{$sport->name}}">{{$sport->name}}</label>
								</div>
								@if ($loop->index + 1 % 4 == 0) <div class="clearfix"></div> @endif
							@endforeach
							<div class="clearfix"></div>
							<div align="right">
								<button class="btn btn-success" type="submit">Search</button>
							</div>
						</form>
			        </div>
		        </div>
	        <div align="right">
		        {!! $userlist->links() !!}
	        </div>
	        @foreach($userlist as $user)
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                    <strong>{{$user->first_name . ' ' . $user->last_name}}</strong>
	                     <span class="text-muted small">{{$user->age}} years old</span>
	                 </div>
	                <div class="panel-body">
	                    {{$user->first_name}}@if ($user->user->sports->count() == 0) does not workout. @else partakes in @endif
	                    @foreach($user->user->sports as $sport)
	                        {{$sport->sport->name}}@if (!$loop->last), @else. @endif
	                    @endforeach
	                </div>
	            </div>
	        @endforeach

	        <div align="right">
		        {!! $userlist->links() !!}
	        </div>
        </div>
    </div>
</div>
@endsection
